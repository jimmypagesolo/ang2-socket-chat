import * as express from "express";
import * as http from "http";
import * as serveStatic from "serve-static";
import * as path from "path";
import * as dotenv from "dotenv";
import * as socketIo from "socket.io";
import * as mongoose from "mongoose";

// add ss
import * as superscript from "superscript";
import * as facts from "sfacts";

//import { RoomSocket } from "./room-socket";

declare var process, __dirname;

//var factSystem      = facts.create('telnetFacts');
/**
 * The server.
 *
 * @class Server
 */
class Server {
    public app: any;
    private server: any;
    private io: any;
    private root: string;
    private port: number;
	private options: any = {};
	// ss engine
	private ssegine: any;
	// botname
	private botname: string;

    /**
    * Bootstrap the application.
    *
    * @class Server
    * @method bootstrap
    * @static
    * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
    */
    public static bootstrap(): Server {
        return new Server();
    }

    /**
    * Constructor.
    *
    * @class Server
    * @constructor
    */
    constructor() {
        // Create expressjs application
        this.app = express();

        // Configure application
        this.config();

        // Setup routes
        this.routes();

        // Create server
        this.server = http.createServer(this.app);

        // Create database connections
        this.databases();
		
		// start the engine
		this.run();
        // Handle websockets
        //this.sockets();

        // Start listening
        //this.listen();
    }

    /**
     * Configuration
     *
     * @class Server
     * @method config
     * @return void
     */
    private config(): void {
        // Read .env file (local development)
        dotenv.config();
        // By default the port should be 5000
        this.port = process.env.PORT || 3500;
        // root path is under ../../target
        this.root = path.join(path.resolve(__dirname, '../../target'));
		this.botname = "@BANKBOT";
    }

    /**
     * Configure routes
     *
     * @class Server
     * @method routes
     * @return void
     */
    private routes(): void {
        let router: express.Router;
        router = express.Router();

        // Static assets
        this.app.use('/assets', serveStatic(path.resolve(this.root, 'assets')));

        // Set router to serve index.html (e.g. single page app)
        router.get('/', (request: express.Request, result: express.Response) => {
            result.sendFile(path.join(this.root, '/index.html'));
        });

        // Set app to use router as the default route
        this.app.use('*', router);
    }

    /**
     * Configure databases
     *
     * @class Server
     * @method databases
     * @return void
     */
    private databases(): void {
        // MongoDB URL
        let mongoDBUrl = process.env.MONGODB_URI || 'mongodb://172.10.16.13/chatbotlogs';

        // Get MongoDB handle
        mongoose.connect(mongoDBUrl);
    }

    /**
     * Configure sockets
     *
     * @class Server
     * @method sockets
     * @return void
     */
    private sockets(err:any, botins:any): void {
        // Get socket.io handle
        this.io = socketIo(this.server);
		// init bot handler
		this.bothandle(err, botins, this.io);
        //let roomSocket = new RoomSocket(this.io);
		
		// on connection based on user
		let msgsock = new MessageSocket(this.io, room.name);
    }
	
	/**
     * Configure sockets
     *
     * @class Server
     * @method bothandle
     * @return void
     */
	private bothandle(err:any, bot:any, _io:any): void {
		_io.on('connection', function(socket) {
			console.log("Visitor --> <'" + socket.id + "'> has connected.\n"); // socket -> session id
			// say hello on new user joined chat
			socket.on('new user', function(msg) {
				bot.reply(socket.id, "", function(err, resObj) {
					if(err) console.log('err -> ', err);
					socket.emit('chat message', { username: this.botname , text: "[<] Hello, " + msg.username + "! \n May i help you?", isbot: true });
				});
			});
			//socket.emit('chat message', { text:"You're connected!" });
			socket.on('chat message', function(msg){
				var message = msg.message;
				var username = msg.username;
				console.log('visitor <@' + username + '> is chatting!');
				// Emit the message of the visitor back first
				socket.emit('chat message', { username: username, text: "[>] " + message, isbot: false });
				bot.reply(socket.id, message.trim(), function(err, resObj) {
					if(err) console.log('err -> ', err);
					var reply;
					if(typeof resObj.string !== typeof undefined || resObj.string != false) {
						reply = "[<] " + resObj.string + ".";
					} else {
						reply = "[<] I'm sorry, <@" + username + ">! I dont know the command.";
					}
					socket.emit('chat message', { username: this.botname, text: reply, isbot: true });
				});
			});
		});
	}
    
    /**
     * Start HTTP server listening
     *
     * @class Server
     * @method listen
     * @return void
     */
    private listen(): void {
        //listen on provided ports
        this.server.listen(this.port);

        //add error handler
        this.server.on("error", error => {
            console.log("ERROR", error);
        });

        //start listening on port
        this.server.on("listening", () => {
            console.log('==> Listening on port %s.', this.port, this.port);            
        });

    }
	
	/**
     * Main method to run all in one
     *
     * @class Server
     * @method listen
     * @return void
     */
	private run(): void {
		// create the facts system
		this.options['factSystem'] = facts.create('telnetFacts');
		this.options['mongoose'] = mongoose;
		
		// Main entry point
		this.ssegine = new superscript(this.options, function(err, botInstance){
			if(err) {
				console.log('Opps! Error has occured => ', err);
			}
			// then, start the socket
			this.sockets(null, botInstance);
			// listen on server
			this.listen();
		});
	}
}

// Bootstrap the server
let server = Server.bootstrap();
export = server.app;